# kin_feas_filter

A kinematic feasibility filter, written in C++, for use with a generic robot arm in MoveIt.

We recommend using IKFast.

Poke around in the code to see where to parallelize with OpenMP. I've found the best configuration to have:
* IKFast
* no OpenMP integration
* Explicit core affinity with the launch arg `"launch-prefix="taskset -c 1"` for cache hits